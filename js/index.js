/* NO, there is NO JS, just a comment
Clicking the *Play!* button starts the animation. Needs support for `transform-style: preserve-3d` (so no IE, not even 11, sorry). Tested & works on Win 7 in Chrome 34 Canary, Chrome 32, Opera 19, Firefox 26 (choppy animation & not so great rendering - original version actually had twice the number of elements, but changed that due to how it behaved in FF). 

It's a torus (http://en.wikipedia.org/wiki/Torus) with pentagonal rings whose centres are distributed along a hypocycloid (http://en.wikipedia.org/wiki/Hypocycloid) with 5 cusps in the `xOz` plane. The pentagonal rings rotate and so does the torus itself.
*/